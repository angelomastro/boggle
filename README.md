# Boggle #

This project solves the game of boggle (*) using a Trie self-made structure in python. The project assumes that you have generated the array to solve and updated the dictionary (Trie) of words in the buggle.py to test. 

* run the game with 
	python buggle.py 		#I used python 2.7

* edit the grid of the game inside the buggle.py to do more testing


----- 
(*) Boggle game: find the longest word in an array of chars. You can build the word moving right, left, up and down in contiguous cells, but cannot reuse the same cells twice. 