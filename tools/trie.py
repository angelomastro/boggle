"""
Implement Trie in Python.
 
Adapted from a set of functions in http://www.geeksforgeeks.org/trie-delete/
"""
 
class Trie:
    def __init__(self, *args):
        self._obj = {}
        for word in args:
            if type(word) != str:
                raise TypeError("Trie only works on str!")
            temp_trie = self._obj
            for letter in word:
                temp_trie = temp_trie.setdefault(letter, {})
            temp_trie = temp_trie.setdefault('_end_', '_end_')

    def has(self, word):
        if type(word) != str:
            raise TypeError("Trie only works on str!")
        temp_trie = self._obj
        for letter in word:
            if letter not in temp_trie:
                return False
            temp_trie = temp_trie[letter]
        return True

    def remove(self, word, dept):
        self.__removeRec(word, dept, self._obj)

    def __removeRec(self, word, depth, obj_trie):
        if word and word[depth] not in obj_trie:
            return False
        if len(word) == depth + 1:
            del obj_trie[word[depth]]
            if not obj_trie:  # Node becomes a leaf, indicate its parent to delete it.
                return True
            return False
        else:
            temp_trie = obj_trie
            if self.__removeRec(word, depth + 1, temp_trie[word[depth]]): # Recursively climb up to delete.
                if temp_trie:
                    del temp_trie[word[depth]]
                return not temp_trie
        return False


 
if __name__ == '__main__':
    test = Trie('hello', 'abc', 'baz', 'bar', 'barz')
    print test
 
    print test.has('hello')
    print test.has('bar')
    print test.has('bab')
    print test.has('zzz')
 
    test.remove('abc', 0)
    print test
    test.remove('hello', 0)
    print test
    test.remove('bar', 0)
    print test
 
    #print test.has(1)
