from tools.trie import Trie

grid = [["t", "h", "e", "r"], 
		["h", "r", "l", "p"], 
		["g", "e", "c", "z"], 
		["x", "a", "d", "d"]]

dictionary = Trie("help", "the", "thread", "read")

def initUsedGrid(grid):
	ret = list()
	for row in grid:
		newCol = list()
		for col in row:
			newCol.append(False)
		ret.append(newCol)
	return ret		

def findMaxString(grid, 
				  dictionary, 
				  usedGrid,
				  row=0, 
				  col=0, 
				  curWord=[""],
				  maxWord=[""]):

	newWord = [ curWord[0] + grid[row][col] ]
	if dictionary.has(newWord[0]):
		if len(newWord[0]) > len(maxWord[0]):
			maxWord[0] = newWord[0]
	else:
		return maxWord[0]

	nextNewWord = [""]
	usedGrid[row][col] = True
	#go right
	if col < len(grid[row])-1 and not usedGrid[row][col+1]:
		nextNewWord[0] = findMaxString(grid, dictionary, usedGrid, row, col+1, newWord, maxWord)
		if (len(nextNewWord[0]) > len(maxWord[0])):
			maxWord[0] = nextNewWord[0]

	# go left
	if col > 1 and not usedGrid[row][col-1]:
		nextNewWord[0] = findMaxString(grid, dictionary, usedGrid, row, col-1, newWord, maxWord)
		if (len(nextNewWord[0]) > len(maxWord[0])):
			maxWord[0] = nextNewWord[0]

	# go up
	if row > 1 and not usedGrid[row-1][col]:
		nextNewWord[0] = findMaxString(grid, dictionary, usedGrid, row-1, col, newWord, maxWord)
		if (len(nextNewWord[0]) > len(maxWord[0])):
			maxWord[0] = nextNewWord[0]
	#go down
	if row < len(grid)-1 and not usedGrid[row+1][col]:
		nextNewWord[0] = findMaxString(grid, dictionary, usedGrid, row+1, col, newWord, maxWord)
		if (len(nextNewWord[0]) > len(maxWord[0])):
			maxWord[0] = nextNewWord[0]


	return maxWord


print findMaxString(grid, dictionary, initUsedGrid(grid))
